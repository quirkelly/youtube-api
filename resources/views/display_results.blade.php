@if($search_results)
    @foreach($search_results as $result)
    <div class="col-md-4">
        <div class="bg-dark shadow-sm mx-auto" style="width: 100%; border-radius: 21px 21px 0 0; text-align:center">
            <img width="320px" height="180px" src="{{ $result->snippet->thumbnails->medium->url }}">
        </div>
        <div class="card mb-4 shadow-sm ex1">        
            <div class="card-body">
                <strong class="d-inline-block mb-2 text-success"><a href="https://youtu.be/{{ $result->id->videoId }}">{{ Str::limit($result->snippet->title, 30) }}</a></strong>
                <p class="card-text ex1" data-placement="top" id="{{ $result->id->videoId }}" data-popid="{{ $result->id->videoId }}" data-toggle="popover" title="{{ $result->snippet->title }}" data-content="{{ $result->snippet->description }}">{{ Str::limit($result->snippet->description, 80) }}<</p>
                <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                        <a href="https://youtu.be/{{ $result->id->videoId }}" class="btn btn-info" role="button">View</a>
                    </div>
                    <small class="text-muted" id="video_duration_{{ $result->id->videoId }}">
                        <a class="btn btn-primary duration" data-video="{{ $result->id->videoId }}" data-toggle="collapse" href="#{{ $result->id->videoId }}" role="button" aria-expanded="false" aria-controls="{{ $result->id->videoId }}">
                            Show Duration
                        </a>
                    </small>
                </div>
            </div>
        </div>
    </div>
    @endforeach
@endif