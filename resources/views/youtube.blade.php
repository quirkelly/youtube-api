@extends('layouts.app', ["current" => "search" ])
@section('body')
<div class="form-row d-flex text-center pt-4">	
	<div class="col-lg-12">
		<h2><span class="label label-primary">YouTube search via the Google (v3) api</span></h2>
	</div>
</div>
<form class="form-horizontal" id="search">
	<div class="form-row d-flex justify-content-center pt-4">
		<div class="col-lg-1"></div>
		<div class="form-group col-md-2 col-lg-2">
			<label for="inputTerm">Search Term</label>
			<input class="form-control mr-sm-2" type="text" name="term" id="term" placeholder="Search Term" required>
		</div>
		<div class="form-group col-md-2 col-lg-2">
			<label for="inputRes">Max Results</label>
			<input class="form-control" type="number" id="max_results" name="max_results" min="1" max="50" step="1" placeholder="Max Results" required>
		</div>
		<div class="form-group col-md-2 col-lg-2">
			<label for="inputOrder">Order</label>
			<select name="order" class="form-control" required id="order_id" name="order_id">
				<option value="" selected="selected">-- Order By --</option>
				<option value="title">Title</option>
				<option value="relevance">Relevance</option>
				<option value="date">Date</option>
				<option value="viewCount">Views</option>
				<option value="rating">Rating</option>		
			</select>
		</div>
		<div class="form-group col-md-2 p-4 mt-2 col-lg-2">
			<button class="btn btn-success" type="submit" ><i class="fa fa-search"></i> Search </button>
		</div>
		<div class="col-lg-1"></div>
	</div>
</form>
@endsection 
     
@section('javascript')
<script type="text/javascript">   
    $(".element").click(function () {
		var popId = $(this).data("popid");
		$('#'+popId).popover('toggle');
	});
	
    $("#results").hide();
	$('[data-toggle="tooltip"]').tooltip()
	$("#search").submit( function(event){ 
		
		event.preventDefault(); 
        parameters = { 
            keyword: $("#term").val(), 
            results: $("#max_results").val(), 
            order: $("#order_id").val()
		};  
        $.ajax({
			type: "GET",
			data: parameters,
			url: "/youtube/search",
			success: function(msg){
				$("#results").show();
				$("#display_res").html(msg);
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				alert("Error loading vidoes");
			}
		});
	}); 
	
	$(".topic").click(function () {
		var cat = $(this).data("category");
		parameters = { 
            keyword: $("#keyword_id").val(), 
            results: $("#max_results").val(), 
            order: $("#order_id").val()
		};
		$.ajax({
			type: "GET",
			url: "/youtube/category/"+cat,
			success: function(msg){
				$("#results").show();
				$("#display_res").html(msg);
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				alert("Error loading vidoes");
			}
		});
	});
	$(document).on("click",".duration",function() {
        var videoId = $(this).data("video");
		
		$.ajax({
			type: "GET",
			url: "/youtube/duration/"+videoId,
			success: function(duration){
                duration = duration.replace('H',' hour ');
                duration = duration.replace('PT','');
                duration = duration.replace('M',' mins. ');
                duration = duration.replace('S',' sec. ');
				$("#video_duration_"+videoId).html(duration);
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				alert("Error extracting video duration");
			}
		});
    });
	
</script>
@endsection