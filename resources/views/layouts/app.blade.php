<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  	<head>
        <title>{{ config('app.name', 'Laravel') }}</title>
        <!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <!-- FontAwesome -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <style>
            p.ex1 {
                min-height: 80px;
            }
        </style>
  	</head>
    <body>
        <header>
            <div class="navbar shadow-sm">
                @component('component_navbar', [ "current" => $current ]) @endcomponent
            </div>
        </header>
        <main role="main">
            <section class="jumbotron text-center">
                <div class="container">
                    @hasSection('body')
                        @yield('body')
                    @endif
                </div>
            </section>
            <div class="album bg-light">
                <div class="container pb-4" id="results">
                    @component('component_result') @endcomponent
                </div>
            </div>
        </main>
        <div class="container pb-5"></div>
        <footer class="navbar fixed-bottom text-muted pt-3 bg-dark">
            @component('component_footer') @endcomponent
        </footer>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        @hasSection('javascript')
            @yield('javascript')
        @endif
    </body>
</html>