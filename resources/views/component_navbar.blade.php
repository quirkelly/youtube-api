<div class="container d-flex justify-content-between">
 	 <h5 class="my-0 mr-md-auto font-weight-normal">
		<a href="/"><i class="fa fa-youtube" style="font-size:48px;color:red"></i></a>
  	</h5>
	<nav class="my-2 my-md-0 mr-md-3">
		<a href="#" data-category="world" class="p-2 text-muted topic">World</a>
		<a href="#" data-category="travel" class="p-2 text-muted topic">Travel</a>
		<a href="#" data-category="technology" class="p-2 text-muted topic">Technology</a>
		<a href="#" data-category="business" class="p-2 text-muted topic">Business</a>
		<a href="#" data-category="politics" class="p-2 text-muted topic">Politics</a>
		<a href="#" data-category="sport" class="p-2 text-muted topic">Sport</a>
		<a href="#" data-category="news" class="p-2 text-muted topic">News</a>
		<a href="#" data-category="music" class="p-2 text-muted topic">Music</a>
	</nav>
	<a class="btn btn-primary" href="https://console.cloud.google.com/marketplace/details/google/youtube.googleapis.com">Console</a>
</div>