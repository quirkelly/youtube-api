<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|


Route::get('/', function () {
    return view('welcome');
});
*/
Route::resource('/', 'YouTubeController');
Route::get('/youtube/search', 'YouTubeController@search_videos');
Route::get('/youtube/category/{clicked}', 'YouTubeController@load_category_videos');
Route::get('/youtube/duration/{video}', 'YouTubeController@get_video_duration');