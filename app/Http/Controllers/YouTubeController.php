<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Google_Client;
use Google_Service_YouTube;

class YouTubeController extends Controller 
{

	/**
	 * Main index function to control controller actions
	 * @return type
	 */
	public function index() {
		/*
		
		$youtube_key = getenv('YOUTUBE_API_KEY');
		$client = new Google_Client();
		$client->setDeveloperKey($youtube_key);
		
		$youtube = new Google_Service_YouTube($client);
		$searchResponse = $youtube->search->listSearch('id,snippet', array(
			'q' => "net ninja",
			'maxResults' => "25",
		));
		
		dd($searchResponse);

		*/
		return view('youtube');
	}

	function get_video_duration($video){		
		try {
			$apikey = getenv('YOUTUBE_API_KEY');
			$dur = file_get_contents("https://www.googleapis.com/youtube/v3/videos?part=contentDetails&id=$video&key=$apikey");
			$duration =json_decode($dur, true);
			return $duration['items'][0]['contentDetails']['duration'];
		} catch(Exception $e){
			return json_encode(['error'=>true, 'message' => $e->getMessage()]);
		}
	}

	public function load_category_videos($cat) {		
		try {
			$youtube_key = getenv('YOUTUBE_API_KEY');
			$client = new Google_Client();
			$client->setDeveloperKey($youtube_key);
			
			$youtube = new Google_Service_YouTube($client);
			$searchResponse = $youtube->search->listSearch('id,snippet', array(
				'q' => $cat,
				'maxResults' => "50",
				'videoDuration' => "any",
				'type' => "video",
			));

			$search_results = $searchResponse['items'];
			return view('display_results', compact('search_results'));
		} catch(Exception $e){
			return json_encode(['error'=>true, 'message' => $e->getMessage()]);
		}
	}

	public function search_videos(Request $request) {		

		$validatedData = $request->validate([
			'keyword' => 'required',
			'results' => 'required|numeric',
			'order' => 'required'
		]);
		
		try {
			$youtube_key = getenv('YOUTUBE_API_KEY');
			$client = new Google_Client();
			$client->setDeveloperKey($youtube_key);
			
			$youtube = new Google_Service_YouTube($client);
			$searchResponse = $youtube->search->listSearch('id,snippet', array(
				'q' => $request->keyword,
				'maxResults' => $request->results,
				'order' => $request->order
			));

			$search_results = $searchResponse['items'];
			return view('display_results', compact('search_results'));
		} catch(Exception $e){
			return json_encode(['error'=>true, 'message' => $e->getMessage()]);
		}
	}

}
